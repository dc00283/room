package main

import (
	"fmt"
	"net/http"

	"gitlab.com/AndyRN/room/src/envar"
	"gitlab.com/AndyRN/room/src/pin"
	"gitlab.com/AndyRN/room/src/website"
)

var (
	// ListenAddress represents the default port for the server to listen on
	ListenAddress = ":8080"
)

func main() {
	fmt.Println("--- Starting Realtime Office Occupation Monitor ---")

	// Update application settings if there are environment variables present
	ListenAddress = envar.UpdateSetting(ListenAddress, envar.ListenAddress)
	website.StaticDirectory = envar.UpdateSetting(website.StaticDirectory, envar.StaticDirectory)
	website.ViewDirectory = envar.UpdateSetting(website.ViewDirectory, envar.ViewDirectory)

	go pin.Monitor()
	a := website.NewDefaultAppRouter()

	server := &http.Server{
		Addr:    ListenAddress,
		Handler: a.Router,
	}

	fmt.Println("Listening on", ListenAddress)
	if err := server.ListenAndServe(); err != nil {
		panic(err)
	}
}
