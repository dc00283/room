package envar

import (
	"fmt"
	"os"
)

const (
	// ListenAddress gets the port for the server to listen on
	ListenAddress = "ROOM_LISTEN_ADDRESS"

	// StaticDirectory gets the path to the static directory
	StaticDirectory = "ROOM_STATIC_DIRECTORY"

	// ViewDirectory gets the path to the view directory
	ViewDirectory = "ROOM_VIEW_DIRECTORY"
)

// UpdateSetting will overwrite the provided setting if the relevant envar is set
func UpdateSetting(setting, env string) string {
	value := getEnvironmentVariable(env)
	if value != "" {
		setting = value
	}
	return setting
}

func getEnvironmentVariable(env string) string {
	value := os.Getenv(env)
	if value == "" {
		fmt.Println("Environment variable not set:", env)
	}
	return value
}
