# ROOM - Realtime Office Occupation Monitor

## Table of Contents

* [Overview](#overview)
* [Setup](#setup)
    * [Prerequisites](#prerequisites)
    * [Getting the Source](#getting-the-source)
* [Project Structure](#project-structure)
    * [Vendor Information](#vendor-information)
* [Deployment](#deployment)

## Overview

Avcodeo project.

## Setup

### Prerequisites

Ensure you have the following installed:

* [Go](https://golang.org/dl/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [SourceTree](https://www.sourcetreeapp.com/) (or preferred Git tool)

### Getting the Source

* Ensure your [GOPATH](https://golang.org/doc/code.html#GOPATH) is set
* Ensure your PATH is updated to include `%GOPATH%\bin`
* Ensure the folder structure exists: `%GOPATH%\src\gitlab.com\AndyRN\room`
* Clone the repository into the `room` directory using the "Source Path / URL": `git@gitlab.com:AndyRN/room.git`.

## Project Structure

* `.gitlab` - Gitlab specific files
* `src` - Source of the project
    * `cmd` - Runnable applications
        * `room` - The web server application
    * `pin` - Handle the GPIO Pi Zero W pins
    * `website` - Root of the website
* `vendor` - Third party libraries

### Vendor Information

* [chi](https://github.com/go-chi/chi) - Router
* [go-rpio](https://github.com/stianeikeland/go-rpio) - Raspberry Pi GPIO

## Deployment

* Ensure you have installed the `rake` package: `go install ./src/cmd/rake`.
* Build the source ready to be deployed to the Pi: `rake`.
